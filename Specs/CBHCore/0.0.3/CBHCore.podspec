Pod::Spec.new do |spec|
  spec.name         = "CBHCore"
  spec.version      = "0.0.3"
  spec.summary      = "Core for CHB projects"
  spec.description  = "Add this to CBH projects"
  
  spec.homepage = "https://bitbucket.org/alterplay/cbh-core-ios/src/develop/"

  spec.source_files = "CBHCore", "CBHCore/**/*.{h,m,swift}"

  spec.author = { "Nikita Pankiv" => "nik.pankiv@alterplay.com" }
  
  spec.platform = :ios, "12.0"
  spec.swift_version = '5.0'

  spec.source = { :git => "git@bitbucket.org:alterplay/cbh-core-ios.git", :tag => "#{spec.version}" }
  
  spec.source_files  = "CBHCore", "CBHCore/**/*.{h,m,swift}"
  
  # Networking
  spec.dependency 'Vox'
  spec.dependency 'Vox/Alamofire'

end
